## Getting started

### Install
` npm install --save godaddy`

### Simple Example Query
```js
  var options = {
    client_id: '(KEY)',
    client_secret: '(Secret)'
  };
  var godaddy = require('godaddy')(options);
  var callback = function(err, data){
    if(err){
        console.log(err);
    } else{
        console.log(data);
    }
  };
godaddy.domains.available({domain: 'google.com'},callback)
```

## Methods

#### godaddy.domains.suggest

```js
  godaddy.domains.suggest({query: 'google.com'}, callback);
```

#### godaddy.domains.available

```js
  godaddy.domains.available({domain: 'google.com'},callback)
```

#### godaddy.domains.bulkAvailable(options, callback)

```js
  godaddy.domains.bulkAvailable({domains: ['domain1.com','domain2.com']}, callback)
```

### Currently undocumented

godaddy.domains.purchase

godaddy.domains.getAgreements

godaddy.shoppers.createSubAccount

## Command Line

A command line tool is also provided that allows you to interact with Godaddy API.

``` bash
  $ ./cli.js
  Godaddy > help

    Commands:

      help [command...]                     Provides help for a given command.
      exit                                  Exits application.
      set keys <id> <secret>
      domains suggest [options]
      domains available [options]
      domains bulkAvailable [options]
      domains purchase [options]
      domains getAgreements [options]
      domains details [options]
      domains renew [options]
      domains list [options]
      shoppers createSubAccount [options]
      shoppers details [options]
```

## Contributors

* Emil Sedgh
* Matt Green
